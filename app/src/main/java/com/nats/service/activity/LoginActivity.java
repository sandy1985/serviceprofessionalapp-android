package com.nats.service.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.nats.service.R;

public class LoginActivity extends AppCompatActivity {
    ImageView iv_user,iv_professional;
    String userType = "user";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        iv_user = (ImageView) findViewById(R.id.iv_user);
        iv_professional = (ImageView) findViewById(R.id.iv_professional);
    }


    public void user(View view){
        iv_user.setImageDrawable(getResources().getDrawable(R.drawable.select_circle));
        iv_professional.setImageDrawable(getResources().getDrawable(R.drawable.white_circle));
        userType = "user";
    }

    public void professional(View view){
        iv_user.setImageDrawable(getResources().getDrawable(R.drawable.white_circle));
        iv_professional.setImageDrawable(getResources().getDrawable(R.drawable.select_circle));
        userType = "professional";
    }
    public void signIn(View view){
       startActivity(new Intent(LoginActivity.this,HomeActivity.class));
    }
}
