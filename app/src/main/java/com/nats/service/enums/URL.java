package com.nats.service.enums;

public enum URL {
    FB_LOGIN("Api/facebookLogin"),
    UPDATE_USER_INFO("Api/updateUserInfo"),
    CATEGORY_Q_LIST("Api/categoryQuestionList"),
    HEADTOHEAD_LIST("Api/headToHeadCategoryList"),
    TRIVIA_NIGHT_PROFILE("Api/profile"),
    WEEKLY_PROFILE("Api/weeklyProfile"),
    CREATE_GAME("TriviaNight/createGame");

    public String mURL;
    public String BaseUrl = "https://whami.whereami.world/";

    URL(String mURL) {
        this.mURL = this.BaseUrl + mURL;
    }

    public String getURL() {
        return mURL;
    }

}
