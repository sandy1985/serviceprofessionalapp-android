package com.nats.service.application;

import android.content.Context;
import android.content.SharedPreferences;

public class AppSettings {
    private SharedPreferences sharedPreferences = null;
    public String __uId, __uName, __uImage, __uEmail, __fcmToken,__friends,__lat,__lng;
    public boolean __isLoggedIn = false;

    public AppSettings(Context context) {
        sharedPreferences = context.getSharedPreferences(Constant.APP_SETTING_KEY.APP_SETTING.name(), Context.MODE_PRIVATE);
        __uName = sharedPreferences.getString(Constant.APP_SETTING_KEY.PREFERENCE_LOGIN_UNAME.name(), __uName);
        __isLoggedIn = sharedPreferences.getBoolean(Constant.APP_SETTING_KEY.IS_USER_LOGIN.name(), __isLoggedIn);
        __uEmail = sharedPreferences.getString(Constant.APP_SETTING_KEY.PREFERENCE_EMAIL.name(), __uEmail);
        __uImage = sharedPreferences.getString(Constant.APP_SETTING_KEY.PREFERENCE_USER_IMAGE.name(), __uImage);
        __fcmToken = sharedPreferences.getString(Constant.APP_SETTING_KEY.FCM_TOKEN.name(), __fcmToken);
        __uId = sharedPreferences.getString(Constant.APP_SETTING_KEY.PREFERENCE_LOGIN_UID.name(), __uId);
        __friends = sharedPreferences.getString(Constant.APP_SETTING_KEY.PREFERENCE_FB_FRIENDS.name(), __friends);
        __lat = sharedPreferences.getString(Constant.APP_SETTING_KEY.LAT.name(), __lat);
        __lng = sharedPreferences.getString(Constant.APP_SETTING_KEY.LONG.name(), __lng);
    }

    public void setSession(String __uId, String __uName, String __uEmail, String __uImage, boolean __isLoggedIn,String __friends) {
        this.__uId = __uId;
        this.__uName = __uName;
        this.__uEmail = __uEmail;
        this.__uImage = __uImage;
        this.__isLoggedIn = __isLoggedIn;
        this.__friends = __friends;


        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Constant.APP_SETTING_KEY.PREFERENCE_LOGIN_UID.name(), __uId);
        editor.putString(Constant.APP_SETTING_KEY.PREFERENCE_LOGIN_UNAME.name(), __uName);
        editor.putString(Constant.APP_SETTING_KEY.PREFERENCE_EMAIL.name(), __uEmail);
        editor.putString(Constant.APP_SETTING_KEY.PREFERENCE_USER_IMAGE.name(), __uImage);
        editor.putString(Constant.APP_SETTING_KEY.PREFERENCE_FB_FRIENDS.name(), __friends);
        editor.putBoolean(Constant.APP_SETTING_KEY.IS_USER_LOGIN.name(), __isLoggedIn);
        editor.commit();
    }

    public void setSession(String __fcmToken) {
        this.__fcmToken = __fcmToken;
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Constant.APP_SETTING_KEY.FCM_TOKEN.name(), __fcmToken);
        editor.commit();
    }
    public void setSession(String __fbId,String __friends) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Constant.APP_SETTING_KEY.PREFERENCE_FB_FRIENDS.name(), __friends);
        editor.commit();
    }

    public void setSession(String __lat,String __lng,int lng) {

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Constant.APP_SETTING_KEY.LAT.name(), __lat);
        editor.putString(Constant.APP_SETTING_KEY.LONG.name(), __lng);
        editor.commit();
    }

    public void clearPref() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear().commit();
    }

}
